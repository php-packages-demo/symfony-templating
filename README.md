# symfony/templating

Templating Component. https://packagist.org/packages/symfony/templating

[![PHPPackages Rank](http://phppackages.org/p/symfony/templating/badge/rank.svg)](http://phppackages.org/p/symfony/templating)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/templating/badge/referenced-by.svg)](http://phppackages.org/p/symfony/templating)
* [referenced-by](https://phppackages.org/p/symfony/templating/referenced-by)

## Official documentation
* [*The Templating Component*](https://symfony.com/doc/current/components/templating.html)

### Deprecated...
* [*New in Symfony 4.3: Deprecated the Templating component integration*
  ](https://symfony.com/blog/new-in-symfony-4-3-deprecated-the-templating-component-integration)
  2019-04 Javier Eguiluz

